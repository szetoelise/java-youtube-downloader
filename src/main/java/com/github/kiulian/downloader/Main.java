package com.github.kiulian.downloader;

/*-
 * -----------------------LICENSE_START-----------------------
 * Java youtube video and audio downloader
 * %%
 * Copyright (C) 2019 Igor Kiulian
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * -----------------------LICENSE_END-----------------------
 */















import java.io.FileNotFoundException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.*;
import java.util.regex.*;
import com.github.kiulian.downloader.model.VideoDetails;
import com.github.kiulian.downloader.model.YoutubeVideo;
import com.github.kiulian.downloader.model.formats.AudioFormat;
import com.github.kiulian.downloader.model.formats.Format;
import com.github.kiulian.downloader.model.formats.VideoFormat;
import com.github.kiulian.downloader.model.quality.AudioQuality;
import com.github.kiulian.downloader.model.quality.VideoQuality;
import javafx.application.Application;

import java.io.File;
import java.io.IOException;

public class Main {
    private static File fileVideo;
    private static boolean audioFinish;
    private static boolean videoFinish;
    private static File fileAudio;


    public static void mergeAll(File video,File audio){

    }


    public static File mergeAudioToVideo() throws IOException, InterruptedException {
        File ffmpegExecutable  = new File("C:\\Windows\\ffmpeg.exe");
        File outputDir = new File(System.getProperty("user.dir") );
        String outFileName = fileVideo.getName().replaceFirst("[.][^.]+$", "") + "_output.mp4";
//        if(fileAudio==null){
//            fileAudio = new File("/tmp");
//        }
//
//        if(fileVideo==null){
//            fileVideo = new File("/tmp");
//        }
        System.out.println(fileAudio.getAbsolutePath().toString());

        for (File f : Arrays.asList(ffmpegExecutable, fileAudio, fileVideo, outputDir)) {
            if (! f.exists()) {
                throw new FileNotFoundException(f.getAbsolutePath());
            }
        }

        File mergedFile = Paths.get(outputDir.getAbsolutePath(), outFileName).toFile();
        if (mergedFile.exists()) {
            mergedFile.delete();
        }

        ProcessBuilder pb = new ProcessBuilder(
                ffmpegExecutable.getAbsolutePath(),
                "-i",
                fileAudio.getAbsolutePath(),
                "-i",
                fileVideo.getAbsolutePath() ,
                "-acodec",
                "copy",
                "-vcodec",
                "copy",
                mergedFile.getAbsolutePath()
        );
        pb.redirectErrorStream(true);
        System.out.println("Please wait, merging your audio and video file..");
        Process process = pb.start();
        process.waitFor();
        System.out.println("Deleting video file..");
        fileVideo.delete();
        System.out.println("Moving mp3 to your directory..");
        fileAudio.renameTo(Paths.get(outputDir.getAbsolutePath(), outFileName).toFile());
        if (!mergedFile.exists()) {

            System.out.println("Merged file exists");
            return null;
        }
        return mergedFile;
    }

        public static  boolean mergeAudioVideo(String audio,String video) {
            File f = new File(video);


            //String videoPath = f.getParent().replace("/video/");
            String[] exeCmd = new String[]{"ffmpeg", "-i", audio, "-i", video ,"-acodec", "copy", "-vcodec", "copy", video + ".mp4"};
            System.out.println(exeCmd);
            ProcessBuilder pb = new ProcessBuilder(exeCmd);
            boolean exeCmdStatus = executeCMD(pb);

            return exeCmdStatus;
        } //End doSomething Function

        private static boolean executeCMD(ProcessBuilder pb)
        {
            pb.redirectErrorStream(true);
            Process p = null;

            try {
                p = pb.start();

            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println("oops");
                p.destroy();
                return false;
            }
// wait until the process is done
            try {
                p.waitFor();
            } catch (InterruptedException e) {
                e.printStackTrace();
                System.out.println("woopsy");
                p.destroy();
                return false;
            }
            return true;
        }// End function executeCMD


    public static void main(String args[]) throws Exception {
        System.out.println("Hello from youtube downloader");
        System.out.println(args[0]);
        //System.exit(1);

       //mergeAudioToVideo(new File("C:\\Data\\Research\\java-youtube-downloader\\BERAPA GAJI MEREKA SATU BULAN_ #TernyataBegini Ep.5 _ Cretivox.mp3"),new File("C:\\Data\\Research\\java-youtube-downloader\\BERAPA GAJI MEREKA SATU BULAN_ #TernyataBegini Ep.5 _ Cretivox.mp4"),new File("C:\\Data\\Research\\java-youtube-downloader"),"test.mp4");
        String videoId = getVideoId(args[0]); // https://www.youtube.com/watch?v=abc12345 <---- this is VIDEO_ID
        YoutubeVideo video = YoutubeDownloader.getVideo(videoId);
        YoutubeVideo audio = YoutubeDownloader.getVideo(videoId);

        VideoDetails details = video.details();
        System.out.println(details.title());
        System.out.println(details.viewCount());
        details.thumbnails().forEach(image -> System.out.println("Thumbnail: " + image));

// filtering formats
        List<VideoFormat> videoFormats = video.findVideoWithQuality(VideoQuality.hd720);
        video.findAudioWithQuality(AudioQuality.high);

        List<AudioFormat> audioFormats = video.findAudioWithQuality(AudioQuality.medium);

        audioFormats.forEach(it -> {
            //System.out.println(it.audioQuality() + " AUDIO : " + it.url());
        });

        videoFormats.forEach(it -> {
            //System.out.println(it.videoQuality() + " VIDEO : " + it.url());
        });

// itags can be found here - https://gist.github.com/sidneys/7095afe4da4ae58694d128b1034e01e2
        Optional<Format> formatByItag = video.findFormatByItag(102);
        if (formatByItag.isPresent()) {
            Format it = formatByItag.get();
            System.out.println("ID TAG(22) : " + it.url());

    }

// downloading

        //video.download(videoFormats.get(0), outputDir);
        //System.out.println(outputDir);
// async downloading


        File outputDirVideo = new File(System.getProperty("user.dir") );
        if(outputDirVideo.exists()==false){
            outputDirVideo.mkdirs();
        }

        video.downloadAsync(videoFormats.get(0), false,outputDirVideo, new YoutubeDownloader.DownloadCallback() {
            @Override
            public void onDownloading(int progress) {
                videoFinish = false;
                System.out.printf("Video Downloaded %d%%\n", progress);
            }

            @Override
            public void onFinished(File file) throws IOException, InterruptedException {
                System.out.println("Video Finished file: " + file);
                videoFinish = true;
                fileVideo = file;

                if(videoFinish==true && audioFinish==true){
                    //mergeAudioVideo(fileAudio,fileVideo);
                    mergeAudioToVideo();
                }
                //this.getClass().
            }

            @Override
            public void onError(Throwable throwable) {
                System.out.println("Video Error: " + throwable.getLocalizedMessage());
            }
        });

        File outputDirAudio = new File(System.getProperty("user.dir") );
        if(outputDirAudio.exists()==false){
            outputDirAudio.mkdirs();
        }
        audio.downloadAsync(audioFormats.get(0),true, outputDirAudio, new YoutubeDownloader.DownloadCallback() {
            @Override
            public void onDownloading(int progress) {
                audioFinish = false;

                System.out.printf("Audio Downloaded %d%%\n", progress);
            }

            @Override
            public void onFinished(File file) throws IOException, InterruptedException {
                System.out.println("Audio Finished file: " + file);
                //File f1 = new File(file.toString());
                //File f2 = new File(file.toString() + ".mp3");
                //boolean b = f1.renameTo(f2);
                fileAudio = file;
                audioFinish = true;
                if(videoFinish==true && audioFinish==true){
                    mergeAudioToVideo();
                }
            }

            @Override
            public void onError(Throwable throwable) {
                System.out.println("Audio Error: " + throwable.getLocalizedMessage());
            }
        });

    }

    public static String getVideoId(String videoId) throws Exception {
        String pattern = "^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]";
        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(videoId);
        int youtu = videoId.indexOf("youtu");
        if(m.matches() && youtu != -1){
            int ytu = videoId.indexOf("http://youtu.be/");
            if(ytu != -1) {
                String[] split = videoId.split(".be/");
                return split[1];
            }
            URL youtube = new URL(videoId);
            String[] split = youtube.getQuery().split("=");
            int query = split[1].indexOf("&");
            if(query != -1){
                String[] nSplit = split[1].split("&");
                return nSplit[0];
            } else return split[1];
        }
        return null; //throw something or return what you want
    }


    public static Map<String, String> getQueryMap(String query)
    {
        String[] params = query.split("&");
        Map<String, String> map = new HashMap<String, String>();
        for (String param : params)
        {
            String name = param.split("=")[0];
            String value = param.split("=")[1];
            map.put(name, value);
        }
        return map;
    }
}
